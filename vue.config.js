const dotenv = require('dotenv')
dotenv.config();

module.exports = {
  chainWebpack: config => {
    config.plugin('html').tap(args => {
      args[0].title = 'ADS-B Viewer';
      return args;
    });
  },
  devServer:{
    port:80,
    proxy: {
      '/api': {
        target: 'http://localhost:8088',
        changeOrigin: true,
        pathRewrite: {
          '^/api': '/api'
        }
      }
    }
  }
}