// Import the new Vue constructor which is different from the one in Vue 2.
import { createApp } from 'vue'

// Define your application components and setup here...
// For this example, let's assume you have an App.vue component.
import App from './App.vue'

const app = createApp(App)

app.mount('#app')
